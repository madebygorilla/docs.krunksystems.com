Saltstack quick notes
*********************

Installation
++++++++++++

.. code-block:: shell

    $yum install salt-master -y




Configuration 
-------------

.. code-block:: shell

    # updated configuration salt master configuration
    $vi /etc/salt/master

if you have any confusion with refernce to this then please check `Installation`_
	
	
Quick commands
--------------

.. code-block:: shell

    $salt target state.sls <sls file>


Sample sls files
----------------

**Package**

.. code-block:: yaml

    core-software:
      pkg:
      - installed
      - pkgs:
          - python-dev
          - git
