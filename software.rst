Softwares
*********

a. Centos 5.8

#.  python 2.7 --> 3.4
#.  django 1.6 --> 1.7 
#.  mezzanine 0.3.4
#.  postgresql & mysql ---> mongodb(posgres/postgis)
#.  virtualenv
       - workon 
#.  pycharm (IDE)
       - vagrant 
#.  putty/winscp3
#.  jenkins CI 
	- panda 
#.  Saltstack Automaition configuration tool
#. bitbucket (git)
#. DNS bind 9.1
#. Email server (postfix) 
#. Vagrant (Virtualbox)
#. OpenVZ - virtualization
#. richText
#. memcache server - object cache 
#. nginx webserver 
#. gunicorn 
#. RabbitMQ MessagingQueue
