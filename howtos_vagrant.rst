Quick start with Vagrant
========================

Why vagrant
-----------

Vagrant is dependent on Virtualbox. 

Installation
------------


quick notes 
-----------

Once you have installed vagrant. you just need to run following two commands
which give you fully functional virtual machine.

.. code-block:: bash

    $vagrant init hashicorp/precise32
    $vagrant up 


We can ssh into above machine through following command "$vagrant ssh" from
directory. 

We can remove the machine easily through "$vagrant destroy"


Getting Started 
---------------

#. Any project which use vagrant is to configure Vagrant using Vagrantfile. 
    #. We can define root directory for the project & lots of other vagrant
    configuration details into it. 
    #. We can descripe type of machine which need to run your project. 

#. you can create the Vagrantfile through running the command "$vagrant init".
it will create Vagrantfile on current location. 

Example:
.. code-block:: bash

    $mkdir sample_project 
    $cd sample_project 
    $vagrant init 

Boxes
*****
We can add box information once we have initiated our first project.

.. code-block:: bash

    $vagrant box add hashicorp/precise32
    Above command dowbload box from "https://atlas.hashicorp.com/boxes/search"
    repo.



Up & SSH
********

.. code-block:: bash

    $vagrant up 
    $vagrant ssh


PROVISONING
***********

This is going to other stuff. I'm justing doing testing with it.


