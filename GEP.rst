Gorilla Enhance Proposals (GEPs)
================================
..
	Gorilla Enhance proposal (GEP) designed to maintain all our proposal for madebygorilla.com. GEP is inspired by PEP and Gorilla Team  maintain all standards,  process, planning  through GEP number and each GEP number defined wither process, standards.


Gorilla Project Standard
------------------------

Following document format has been defiend under GEP8. 


.. list-table:: Frozen Delights!
   :header-rows: 1

   * -  Standard file Format 
     -  Description
   * -  `Progress Report`_
     -  individual progress report
   * -  `GEPs`_
     -  if you want apply a new GEPs then you can fill your file in following format and uploaded to shared location after filling up GEP tracket
   * -  `Repositories`_
     -  A standard format
   * -  `Visiting Card`_
     -   Standard vistiting card
   * -  `Letterhead`_
     -  Standard letter head design
   * -  `Logo`_		
     -   Standard madebylogo 
   * -  `fonts, format and colors`_ 	
     -	Standard fonts colors & format which we will follow in our letters & email
   * -  :ref:`Email <Emails-Signature>`
     -    Email Signatures
		

.. _Emails-Signature:

Email Signature
+++++++++++++++

|	Regards
|	Name
|	Designation
|	Email | Phone | Madebygorilla.com
	
		
.. 		
.. _fonts, format and colors: http://docs.madebygorilla.com
.. _Logo: http://docs.madebygorilla.com/		 
.. _Visiting Card: http://docs.madebygorilla.com/		 
.. _Letterhead: http://docs.madebygorilla.com/
.. _Repositories: http://docs.madebygorill.com
.. _GEPs: https://docs.google.com/a/madebygorilla.com/document/d/1TqcICMljaqBDchAj4r730AG87u9julRHAMW6WJO83sw/edit?usp=sharing
.. _Progress Report: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1MKeVWMNmDIA1FuimbvQNEwUp7tH0IdTp3OTidL8EnAQ/edit?usp=sharing	
