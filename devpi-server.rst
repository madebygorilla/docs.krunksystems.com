devpi-server
************


Installation
============

.. [#] Create virtual env project 

.. code-block:: python

    mkproject tmp-devpi

.. code-block:: python

    workon tmp-devpi
    pip install -U devpi-server
    devpi-server --version

.. [#] Install devpi-server with configuration file

.. code-block:: python 

    $workon tmp-devpi
    $devpi-server --gendeploy /project/devpi-server --host 192.168.1.103 --port 8081
    $deactivate
    $rmvirtualenv tmp-devpi

.. [#] Activate devpi-server env 

.. code-block:: python
    
    $mkdir /projects/devpi-server
    $echo "/projects/devpi-server" ~/.virtualenv/devpi-server/.project
    $workon devpi-server
    
.. [#] Manage your service

.. code-block:: python

    $workon devpi-server
    $devpi-ctl status
    $devpi-ctl start all


Client Configuration
====================

.. [#] Command line uses 

.. code-block:: python 

    $pip install -i http://192.168.1.103:8081/root/pypi/+simple django

.. [#] Configure in environment

.. code-block:: python 

    $echo "PIP_INDEX_URL='http://192.168.1.103:8081/root/pypi/+simple'" >> ~/.bashrc

.. [#] Configure in pip file 

.. code-block:: python 
    
    [global]
    index_url=http://192.168.1.103:8081/root/pypi/+simple


Auto start at boot
==================

.. code-block:: bash

    $crontab -e
    #update following entry in corntab file 

    @reboot /root/.virtualenvs/devpi-server/bin/devpi-ctl start all 

