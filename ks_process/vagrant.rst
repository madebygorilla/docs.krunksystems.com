List Of Software
----------------

.. code-block:: shell

    sudo apt-get update
    sudo apt-get install postgresql-9.1
    sudo apt-get install postgresql-server-dev-9.1




How To Configure Vagrant With Master-Less Salt
----------------------------------------------
.. [#] Create Vagrant machine.

.. code-block:: shell

    $vagrant init "hashicorp/precise32"

.. [#] Install vagrant-salt plugin.

 .. code-block:: shell

    $vagrant install plugin vagrant-salt

.. [#] Update the directory structure under vagrant Project file

.. code-block:: shell



.. [#] Configure vagrant salt configuration in *Vagrantfile*

.. code-block:: ruby

    ## For masterless, mount your salt file root
    config.vm.synced_folder "salt/roots/", "/srv/salt/"

    # Provisioning #1: install salt stack
    config.vm.provision :shell,
        :inline => 'wget -O - http://bootstrap.saltstack.org | sudo sh'

    # Provisioning #2: masterless highstate call
    config.vm.provision :salt do |salt|
        salt.minion_config = 'salt/minion'
        salt.run_highstate = true
        salt.verbose = true
    end