How to create repo
------------------

**Create repo on bitbucket**

#. Create a repo on bitbukcet
#. Update the team setting into repo
#. Update deployment keys
#. Update hipchat configuration
#. Update jenkins hooks

**Update the Repositories**

Once our repo is created on bitbcket. We have to follow some extra steps which help us to maintain our Gorilla Development process smooth & clean. 

#. You might already have access on `Documents <docs.madebygorilla.com>`_ . 

If you do not have access then please check your access on `Repositories <https://docs.google.com/a/madebygorilla.com/spreadsheets/d/14eaBwd73nL6vYcGPXeXSW4M44jzR-eAuJHewVAgHbHE/edit?usp=sharing>`_ 

Also check access on `Theme directory <https://drive.google.com/a/madebygorilla.com/#folders/0B9DODrzOTadGTHM1cTMwM3dvTnM>`_

#. Assuming that you have access on `Repositories  <https://docs.google.com/a/madebygorilla.com/spreadsheets/d/14eaBwd73nL6vYcGPXeXSW4M44jzR-eAuJHewVAgHbHE/edit?usp=sharing>`_ .

You have to update the list of column on 'Repositories' like GAID, Worksheets Name & Bit Bucket Information, About Developer & Tester, About the Google Spreadsheet Information and UAT Tracker. 

#. We follow the following standards to generate GAID (Gorilla Application ID) 

	- 2 characters for theme (TH) 
	- 8 characters to represent date in following format (YYYMMDD)
	- 3 digit sequence of IDs generated on the same date (For example if you are updating first theme then your last three digit should be 001 or if it's your second theme which you are updating on 'Repositories' then it should be 002. 

**Example** 

::

	TH20152601001
	TH20152601001

#. Next column repersents worksheet names which we create in other file. So please create/update column 2, 3, 4 as checklist, Information, UAT respectively.

#. On Column 5, (Repo name), Please add repo name which you have created bitbucket. 

#. On Column 6,(Repo url), Please update repo url. 

#. on column 7,(Git protocol url) update git protocol url. 

#. on column 8, Update it as theme 

#. On column 9, 10, 11, you have to update developer, UAT tester, code review information. 

#. Please stop Once you have completed the above. 

Now, you have to create a  Spreadsheet under: 
`Theme <https://drive.google.com/a/madebygorilla.com/#folders/0B9DODrzOTadGTHM1cTMwM3dvTnM>`_ directory with name of GAID which have generated for colum 1. 

You can create a copy of Base spreadsheet which you can find inside theme directory. This file should contain "checklist", "Information", "UAT" which we have defined on point number 2. 

#. Once you have created spreadsheet with name of GAID then fetch the uniqe ID from URL. 

#. Go back to `Repositories  <https://docs.google.com/a/madebygorilla.com/spreadsheets/d/14eaBwd73nL6vYcGPXeXSW4M44jzR-eAuJHewVAgHbHE/edit?usp=sharing>`_ you can past the url id on colum 10 and url in column 11. 

#. You can update the UAT URL with GAID + .madebygorilla.com.

#. on column 13, Please copy data from above cell to here. 






