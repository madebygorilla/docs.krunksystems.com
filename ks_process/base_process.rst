Directory Structure
-------------------

Repository
++++++++++
.. [#]    src\
.. [#]    Vagrantfile
.. [#]    Deployment
.. [#]    .gitignore


How To Start A Current Project
-------------------------------
1. From pycharm tool choose option checkout from VCS and choose the option Git
2. Initialize vagrant
3. Make an entry of .vagrant in .gitignore
4. Add, Commit and Push the changes made.
5. Run Command: 

.. code-block:: shell

    $vagrant up

6. Make changes in project interpreter and select current project.
7. Start SSh session for vagrant machine.

After running vagrant machine, follow the steps:

1). Go to vagrant directory.

2). Go to your project directory.

3). If pip is not installed then run command:

.. code-block:: python

    sudo apt-get install python-pip

4). Then install the following packages to avoid the errors that will occur while installing packages from requirements.txt

.. code-block:: python

    sudo apt-get update

This will update the URLs from which data needs to be fetched

.. code-block:: python

    sudo apt-get install postgresql-9.1
    sudo apt-get install postgresql-9.1-dev
    sudo apt-get install python-dev -y

5). Run command:

.. code-block:: python

    pip install -r requirements.txt




How To Set-Up Any New Project
-----------------------------

1). Start project of django using command:

.. code-block:: shell

    django-admin.py startproject --template=https://bitbucket.org/dmalikcs/django-project-template/get/master.zip -name="Readme.md" project_name

2). Open project in Pycharm

3). Open and modify the database.sls file and update the username and database name.

4). Start vagrant machine.

5). Make settings in project interpreter

6). Go to tools --> Start SSH session

.. code-block:: shell

    cd /vagrant/src/

.. code-block:: python

    pip install -r requirements.txt

If pip is not installed, install it.

.. code-block:: python

    apt-get install python-pip

7). Go to settings --> Languages and frameworks --> Django

8). Select checkbox "Enable Django Support" and select paths for Django project root, settings and manage.


