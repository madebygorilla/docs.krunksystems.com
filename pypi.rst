PYPI package configuration
**************************

How to install python package
+++++++++++++++++++++++++++++

.. code-block:: python
    
    pip install -i http://salt:8080/local [package]
    pip install [package]

*Example*

.. code-block:: python

    pip install -i http://salt:8080/local -r requirements.txt

cache requirement.txt 
+++++++++++++++++++++

.. code-block:: python 
    
    pip freeze | curl -X POST -F requirements=@- http://salt:8080/requirements.txt | python -m json.tool

    
    curl -X POST -F requirements=@requirements.txt http://salt:8080/requirements.txt | python -m json.tool

Upload package to server 
++++++++++++++++++++++++

.. code-block:: python 

    curl -X POST -F sdist=@dist/mypackage-1.0.tar.gz http://salt:8080/uploadpackage/
    
PIP variable
++++++++++++

export PIP_DOWNLOAD_CACHE=/var/cache/pip

.. note:: 
    '/var/cache/pip' path is available through salt NFS server

    Mount package

    mount -t nfs salt:/download/cache/pip /var/cache/pip
    




