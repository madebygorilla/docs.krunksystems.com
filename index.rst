.. Krunk Docs documentation master file, created by
   sphinx-quickstart on Fri Mar 28 17:58:23 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Krunk documentation!
======================================

**Krunk Systems (p) Ltd Document**


Contents:

.. toctree::
   :maxdepth: 2

   django
   git
   saltstack
   pypi
   virtualenv
   devpi-server
   checklist
   unset_env_variable
   howtos_vagrant
   software
   snippets
   progress
   django_custom_application_docs
   urls
   ks_process
   madebygorilla
   GEP

   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

