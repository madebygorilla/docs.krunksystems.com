Important  urls
***************
.. _url-infradetail:

Infrastructure Details
----------------------

#. `Infrastructure Information`_
#. `Soical Profile details`_


.. _url-tracker:

Tracker
-------

#. `Bitbuket Repo`_
#. `Issue Tracker`_
#. `Repositories`_
#. `Branding & SEO Work`_
#. `list of upcoming themes`_
#. `UAT Tracker`_
#. `GEPs Tracker`_


.. _url-dailyProgress:

Daily Progress Report
---------------------

`Rakesh`_	 	
`Anu`_		  
`Pankaj`_
`KB`_	


.. _url-GEPs:

Gorilla Enhance Proposals (GEPs)
--------------------------------



.. _url-postfix:

Postfix online documents
------------------------

#. http://www.linuxmail.info/
#. http://www.linuxmail.info/multiple-domains-postfix-admin-centos-5/
#. http://www.postfix.org/VIRTUAL_README.html#virtual_mailbox
#. http://www.knowplace.org/pages/howtos/smtp_gateway_for_multiple_domains_with_postfix.php
#. http://www.akadia.com/services/postfix_separate_mailboxes.html
#. https://www.digitalocean.com/community/tutorials/how-to-install-and-setup-postfix-on-ubuntu-14-04
#. https://ostechnix.wordpress.com/2013/02/08/setup-mail-server-using-postfixdovecotsquirrelmail-in-centosrhelscientific-linux-6-3-step-by-step/


Sphinx 
------

http://docutils.sourceforge.net/docs/user/rst/quickref.html


.. _Soical Profile details: https://docs.google.com/a/madebygorilla.com/document/d/1W6wvXe3x1OLMDfItQIQVWuZfqy7cOU1GXoVm_cpTm34/edit?usp=sharing
.. _Infrastructure Information: https://docs.google.com/a/madebygorilla.com/document/d/1tBL90JbSymGFU9IfNWbyJh0zY5KGYGSLaz-QcNZTk-o/edit?usp=sharing

.. _Bitbuket Repo: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/14eaBwd73nL6vYcGPXeXSW4M44jzR-eAuJHewVAgHbHE/edit?usp=sharing
.. _Issue Tracker: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/19BXykMsw1tW8BT-YMJudsjjXGJp0VsiGvLZY2yjIapc/edit?usp=sharing
.. _Repositories: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1LcvBoCdALn2UrsJKFnOXw734d9xOd-VCTTTPiYtmp3o/edit?usp=sharing
.. _Branding & SEO Work: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1bu7xW_XqPQbQ48HQzAvCrZToeI0xAJ3Z1LrWOCu5xWY/edit?usp=sharing
.. _list of upcoming themes: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1LcvBoCdALn2UrsJKFnOXw734d9xOd-VCTTTPiYtmp3o/edit?usp=sharing
.. _UAT Tracker: <https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1kfZTvgFmShUv9vaWlhu7W_aPGsxJqG6GMyO24o4LW1M/edit?usp=sharing

.. _GEPs Tracker: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1I5rRJi1cAvnvohsILEMVBA_VdyahEMQHBe493UZ1uRw/edit?usp=sharing



.. _Pankaj: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1fPr0s_S-TLua9I_mwziZfDgVSj23pl-b--BMT0l85ns/edit?usp=sharing
.. _KB: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1_Pjg08lMqfYBbCGFbNXaEG-eS59c6igTv57efHZTI5A/edit?usp=sharing
.. _Rakesh: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1PlzIZ-h6Obk6Z1hB9_hEKd_EMVotDHtH-gCqtsD2vhc/edit?usp=sharing
.. _Anu: https://docs.google.com/a/madebygorilla.com/spreadsheets/d/1Y1MVbe0LxGtvhES4jx5QQSeF3MgC8VoLUbUvhawas50/edit?usp=sharing
