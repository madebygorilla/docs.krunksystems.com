Madebygorilla.com
=================

**HTML Version Madebygorilla.com subscriber welcome message on there email.**

This content will be sent to user once user has subscribe on our subscription link. All the variables will be filled
through code like *{{ email }}* replaced with *dmalikcs@gmail.com*. other variable in similar way.
	

Email Notification Content
--------------------------
Subscription welcome email

Dear {Member},

Thank you for subscribing to MadebyGorilla. We are a brand new web development and CMS platform that will allow you to create and deploy your own business websites with ease. MBG promises to be a step forward in the field of user friendly web builders.
 
MBG is at present under development. We will keep you informed of our progress as we are going to launch soon. MadebyGorilla uses state-of-the-art technology in web design and hosting which guarantees that your sites made on MBG would be faster, smarter and more robust than on any other contemporary web builder platforms available today.

Stay tuned for more news.

Regards

Deepak M.
Co-founder 
MadebyGorilla.com

List of Certificates 
--------------------

	#. etrust 
	
	
List of PR Website 
------------------

	#. timesofindea.com
	
	
List of blogs(headlines or short tagline) 
-----------------------------------------

	#. Top 10 web building  platform 
	#. A win win combination of Web building platform.

Blog Post 3
------------

==============================================
10 biggest challenges in Web Development Today
==============================================

With modern innovations in CMSs and web builders, setting up a website has become a relatively easier task. However when businesses need a dream website, things are not as simple as it is advertised by web builders. The most common outcomes of website projects are:

a. Hire a web developer and end up in a development hell
b. Use a web builder and get caught in the do-it-yourself complications

In either case the websites are seldom up to satisfaction. Discussions on hiring a web developer brings up many questions which includes budget and time management. If quality is the main concern then good web development companies are always preferable. If you choose a reliable web development company they can deliver your dream website but for a big price tag. If you build your own site, it can be wither a disaster or a delight. In any case you should know what are the basic challenges in web development today. So, when you are looking for hiring a web development company you should have these things in mind:

**1. Browser Compatibilities**  -  If you look few years back, there were hardly more than three browsers but now, we have a slew of browsers to deal with. Which means different layout engines, diverse coding protocols, touch sensitive inputs and mobile screen compatibility etc. Earlier developers used to make their website keeping in mind Internet Explorer but now you have to cope up with chrome, UC Browser, Mozilla, rocket or safari with frequent updates.
**2. Different Mobile Platforms** - If you want great visibility for your website. You have to consider this aspect in detail. You have to work upon on your website in such a way that it should be compatible with all the different platform available like Apple iOS, Google Android or Windows Phone. You also have to keep in mind some essentials such as Terms and Conditions, Privacy Policies, User Agreements etc.. 
**3. Design Location Based Features** - As smartphones are getting more and more feature rich, businesses understand the value of accumulating location based data from users. New features and applications are designed to exploit location data for marketing. So, your website should also support these features which can track your visitor's activities as well as give them options to share their location. Now, location based web design also means that your website content should be translated to different languages that exist in the world.
**4. W3C may not be everything** - The consortium guidelines are taken as the commandments of web development. However, since the technologies are evolving at a galactic rate, W3C rules sometimes may not cover a few aspects. W3C specifications for social APIs and image optimization are at present a bit rudimentary. In many ways W3C still follows the old school rules which especially lacks definitions when it comes to handling newer, high- quality media file formats. 
**5. Manage site loading speed** - Sometimes having unique content, great design or few more great things fail to create an impact when your website loads slowly. In traditional web development and web builder CMSs there are limited measures to tackle this issue. Web caching and better HTML framework are valid solutions but beyond a point your site has to shift to high speed hosting. This is usually an expensive option.

**Process related Challenges**

Now, let's look into some of the challenges in web development from the point-of-view of project management. Many of the following issues crop up when businesses go for a web developer firm for their website requirements.
 
**1. Client expectation management** - There are two types of Clients which mainly web development companies come across. First are those who want the best and give full freedom when it comes to budget. Second ones are those who want the best but in a tight budget. Meeting up their expectation can be very tough in both cases.
**2. Ongoing support and maintenance** - After completing the website, the web development companies give full control or admin control in the hands of their clients without proper testing. However, every other day some technical issues arise in the website either due to human error or some other factor. Since most clients lack technical knowledge, they always blame web developer firms and withhold payments. Ongoing maintenance and 24/7 support are always integral to web development.  
**3. Contingency and scope creep** - Whenever a client comes up with any type of website idea which they term as 'just a simple website', it never is. Clients have a tendency to come up with additional features which were not even remotely described in the initial scope of the project. For such times, including a contingency for the project's scope creep is recommended. It should be in the contract and carefully mentioned. 
**4. Optimal testing automation** - Earlier for any repetitive testing, web developers used to put their time as well as efforts for performing manual tests. Things have changed a lot now since automated testing and diagnostic tools  can perform repetitive tasks on their own. Well, these automations are very useful but their costs are pretty high. Automation only becomes cost � effective when it is used for any long term project. So, if the client with small budget has a project which needs these tools then web developers avoid going for such options. Traditional web development suffers a set back here.
**5. Talent pool**  - Today, everyone wants to take their business online which increases the demand of web developers. New web developer firms spring up like weed which disrupts the market for old players. While the lack of talented developers troubles both old and new web development companies, newer more aggressive companies start competing in cost. So, it becomes tougher to deliver quality which in turn restricts talented developers from providing their best. It's a vicious circle.

Seeing all the technical and non-technical challenges in the field of web development, we have figured out some key solutions. We have innovated in integrated web development that will eradicate the gap between web-developer's capability and client's expectations. We are scheduled to launch MadebyGorilla.com soon and we will look forward to you joining us.

