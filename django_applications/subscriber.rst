Subscribe Application
+++++++++++++++++++++

**Clone the application**

.. code-block:: bash
    
    $pwd 
    $mkdir subscriber
    $cd subscriber
    $git archive --format=tar --remote=git@bitbucket.org:dmalikcs/subscriber-apps.git HEAD | tar xf -
    

**update settings_base.py file**

.. code-block:: python 
    
    INSALLED_APPS = (
        ...
        'subscriber',
        ...
    )

**update urls.py file**

.. code-block:: python

    urlpatterns = patterns('',
        ...
        url(r'^subscribe/', include('subscriber.urls')),
        ...
    )

** database sync through django"

.. code-block:: shell

    $python manage.py migrate subscriber


**update HTML domain**

.. code-block:: html

    <div class="col">
        <h4>Mailing list</h4>
        <p>Sign up if you would like to receive occasional treats from us.</p>
        <div id="success"></div>
        <form  class="form-inline">
            <div class="input-group">
                {% csrf_token %}
                <input id="subscribe_email" type="text" class="form-control" placeholder="Your email address..." />
                <span class="input-group-btn">
                    <button id="subscribe" class="btn btn-primary" type="button">Go!</button>
                </span>
            </div>
        </form>
    </div>


**update javascript function**

.. code-block:: javascript

    <script>
   $("button#subscribe").click(
    function () {
        $.ajax({
                datatype:'html',
                type:'POST',
                //contentType: 'application/json',
                url:'/subscribe/subscribe/',
                data: {
                    email: $("input#subscribe_email").val(),
                    csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
                },
                beforeSend: function(){
                    $('input#subscribe_email').val('');
                },
                statusCode: {
                    400: function(){
                        $('div#success').html("Kindly subscribe with valid email id");
                    },
                    200: function(){
                        $('div#success').html("Thank You for Subscribing. Your\
                        email address has been added to our Email Subscription List\
                        and you will start to receive newsletters");
                    }
                },
            }
            ); // ajax function closed
        } //function closed
        ); //click function closed

		</script>

**Update some custom variables in settings_base.html file**

.. code-block:: python 

    SUBCRIBER_TEXT_TEMPLATE = 'email/subscriber.txt'
    SUBCRIBER_HTML_TEMPLATE = 'email/subscriber.html'
    SUBCRIBER_SUBJECT = 'You have been subscribed for krunk news letters'
    SUBCRIBER_FROM_EMAIL = 'support@krunksystems.com'
