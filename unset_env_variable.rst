Unset Environment Variable 
*****************************

Steps
=========

1. Open bash-profile file
	vi ~/.bash_profile 

2. Hash the line where environment variable is set
3. Run command
	
	unset (variable name)


