Git configuration
*****************


Install git on your machine
===========================

.. code-block:: bash

    #yum install git


setup your github account
=========================

.. code-block:: bash

    git config --global user.name rmor
    git config --global user.email "morrakesh@gmail.com"

How to clone a source code from git.

.. code-block:: bash

    git archive --format=tar --remote=git@bitbucket.org:dmalikcs/subscriber-apps.git HEAD | tar xf -


**Some shortcuts which we follow**


.. code-block:: bash

    $git config --global alias.pm 'push origin master'
    $git pm 
    $
    $git config --global alias.st 'status -s'
    $git st










