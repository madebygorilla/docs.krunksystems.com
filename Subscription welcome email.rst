Subscription Welcome
=====================

Dear {Member},

Thank you for subscribing to MadebyGorilla. We are a brand new web development and CMS platform that will allow you to create and deploy your own business websites with ease. MBG promises to be a step forward in the field of user friendly web builders.
 
MBG is at present under development. We will keep you informed of our progress as we are going to launch soon. MadebyGorilla uses state-of-the-art technology in web design and hosting which guarantees that your sites made on MBG would be faster, smarter and more robust than on any other contemporary web builder platforms available today.

Stay tuned for more news.

Regards

Deepak M.
Co-founder 
MadebyGorilla.com





